/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;
import java.util.HashMap;
import util.AnnotationUrl;
import util.ModelView;

/**
 *
 * @author itu
 */
public class Personne {
    
    private String nom;
    private String prenom;
    private Integer age;
    private Date naissance;

    @AnnotationUrl(url_method = "nom")
    public String getNom() {
        if(nom.isEmpty())return nom;
        return "Jean";
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @AnnotationUrl(url_method = "prenom")
    public String getPrenom() {
        if(prenom.isEmpty())return prenom;
        return "Paul";
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @AnnotationUrl(url_method = "age")
    public Integer getAge() {
        if (age != null)return age;
        return 19;
    }
    
    public void setAge(Integer age) {
        this.age = age;
    }
    
    @AnnotationUrl(url_method = "naissane")
    public Date getNaissance() {
        if (naissance != null)return naissance;
        return Date.valueOf("2022-01-01");
    }
    
    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }
    
    @AnnotationUrl(url_method = "profil")
    public ModelView Profil(){
        ModelView mv = new ModelView();
       
        HashMap<String,Object> data = new HashMap<String,Object>();
        data.put("nom", "Jean");
        data.put("prenom", "Paul");
        data.put("age", "19");
        
        mv.setData(data);
        mv.setUrl("profil.jsp");
        
        return mv;
    }
    
    @AnnotationUrl(url_method = "save")
    public ModelView Save(){
        ModelView mv = new ModelView();
       
        HashMap<String,Object> data = new HashMap<String,Object>();
        data.put("nom", this.nom);
        data.put("prenom", this.prenom);
        data.put("age", this.age);
        data.put("naissance", this.naissance);
        
        mv.setData(data);
        mv.setUrl("save.jsp");
        
        return mv;
    }
}
