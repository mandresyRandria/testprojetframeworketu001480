<%-- 
    Document   : profil
    Created on : 16 nov. 2022, 07:49:30
    Author     : itu
--%>
<%
    String[] data = new String[3];
    data[0] = request.getAttribute("nom").toString();
    data[1] = request.getAttribute("prenom").toString();
    data[2] = request.getAttribute("age").toString();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Profil</title>
    </head>
    <body>
        <h1>Profil</h1>
        <p>Nom : <%= data[0] %></p>
        <p>Prénom : <%= data[1] %></p>
        <p>Age : <%= data[2] %></p>
    </body>
</html>
