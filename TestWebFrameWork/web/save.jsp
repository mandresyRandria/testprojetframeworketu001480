<%-- 
    Document   : profil
    Created on : 16 nov. 2022, 07:49:30
    Author     : itu
--%>
<%
    String[] data = new String[4];
    data[0] = request.getAttribute("nom").toString();
    data[1] = request.getAttribute("prenom").toString();
    data[2] = request.getAttribute("age").toString();
    data[3] = request.getAttribute("naissance").toString();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Save</title>
    </head>
    <body>
        <h1>Save</h1>
        <p>Nom : <%= data[0] %></p>
        <p>Prénom : <%= data[1] %></p>
        <p>Age : <%= data[2] %></p>
        <p>Date de naissance : <%= data[3] %></p>
    </body>
</html>
